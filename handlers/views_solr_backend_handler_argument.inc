<?php

class views_solr_backend_handler_argument extends views_handler_argument {
  /**
   * Set up the query for this argument. The argument sent may be found at
   * $this->argument.
   */
  function query($group_by = FALSE) {
    // @todo: Handle group_by argument.
    $this->query->add_argument($this);
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['solr_field'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['solr_field'] = array(
      '#type' => 'textfield',
      '#title' => 'Solr field',
      '#description' => t('The field name in the Solr schema that will be used as the filter.'),
      '#default_value' => $this->options['solr_field'],
      '#required' => TRUE,
    );
  }

  function generate() {
    $xpath = $this->options['solr_field'];
    $value = $this->get_value();

    // Awesome string escape.
    $value = '"' . str_replace('"', '\"', $value) . '"';
    // @todo: Maybe set an extra option so you can select the operator?
    return "$xpath = $value";
  }

  function __toString() {
    return $this->generate();
  }
}
