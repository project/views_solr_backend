<?php

/**
 * @file
 * Query plugin for views_solr_backend.
 */
class views_solr_backend_plugin_query extends views_plugin_query {

  /**
   * Array of parameters for Solr query.
   */
  protected $params;
  protected $query_params;


  /**
   * Array of all encountered errors. Each of these is fatal, meaning that a
   * non-empty $errors property will result in an empty result being returned.
   *
   * @var array
   */
  protected $errors;

  function fetch_results($solr_host, $solr_port, $solr_path, $solr_timeout, $solr_params, $params) {

    require_once(drupal_get_path('module', 'views_solr_backend').'/Library/Solarium/Autoloader.php');
    Solarium_Autoloader::register();

    $config = array(
        'adapteroptions' => array(
            'host' => $solr_host,
            'port' => $solr_port,
            'path' => $solr_path,
            'timeout' => $solr_timeout,
            'params' => $solr_params,
        )
    );

    // create a client instance
    $client = new Solarium_Client($config);

    // create a ping query
    $ping = $client->createPing();

    // execute the ping query
    try {
      $result = $client->ping($ping);
    } catch(Solarium_Exception $e){
      throw new Exception(t('Couldn\'t connect to Solr.'));
    }

    // get a select query instance
    $query = $client->createSelect();

    // Add any additional manually configured parameters.
    $solr_params = array();
    parse_str($this->options['solr_params'], $solr_params);
    foreach ($solr_params as $key => $param) {
      $query->addParam($key, $param);
    }

    // set fields to fetch (this overrides the default setting 'all fields')
    foreach ($this->fields as $key=>$value){
      $fields[]=$key;
    }
    $query->setFields($fields);

    $filters = array();
    if (isset($params['filters']) && isset($params['arguments'])) {
      $filters = array_merge($params['filters'], $params['arguments']);
    }
    elseif (isset($params['filters'])) {
      $filters = $params['filters'];
    }
    elseif (isset($params['arguments'])) {
      $filters = $params['arguments'];
    }

    if (count($filters)) {
      $filter = implode(' AND ', $filters);
      $query->setQuery($filter);
    }

    if (isset($params['sorts'])) {
      foreach ($params['sorts'] as $sort => $order){
        $query->addSort($sort, $order);
      }
    }

    if (isset($params['default_field'])) {
      $query->setQueryDefaultField($params['default_field']);
    }

    // set start and rows param (comparable to SQL limit) using fluent interface
    $query->setStart($params['start'])->setRows($params['rows']);

    $queryString = $query->getQuery();
    $start = microtime(True);
    $resultset = $client->select($query);
    $end = microtime(True);
    $solr_qtime = $resultset->getQueryTime();
    $select_time = $end - $start;
    watchdog('views_solr_backend', t('Solr debug info: The solr query "%query" took %stime seconds to execute, and solr reported a query time of %qtime.', array('%query' => $queryString, '%stime' => $select_time, '%qtime' => $solr_qtime)));

    return $resultset;
  }

  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {

    $view->init_pager();

    // Let the pager modify the query to add limits.
    $this->pager->query();

    $params = array();

    // Defined arguments in the view
    if (isset($this->params['arguments'])) {
      $params['arguments'] = $this->params['arguments'];
    }
    else {
      if (isset($this->pager->view->argument)) {
        $arguments = array();
        foreach ($this->pager->view->argument as $argument) {
          if ($argument->table == 'solr') {
            $arguments[] = $argument->options['solr_field'] . ':' . $argument->argument;
          }
        }
        $params['arguments'] = $arguments;
      }
    }

    // Defined filters in the view
    if (isset($this->params['filters'])) {
      $params['filters'] =  $this->params['filters'];
    }

    // Defined sorts in the view
    if (isset($this->params['sorts'])) {
      $params['sorts'] =  $this->params['sorts'];
    }

    // Defined default field in the view
    if (isset($this->params['default_field'])) {
      $params['default_field'] =  $this->params['default_field'];
    }

    if (isset($this->params['q'])) {
      $params['q'] =  $this->params['q'];
    }

    $params['rows'] = $this->pager->options['items_per_page'];
    $params['start'] = $this->pager->current_page * $this->pager->options['items_per_page'];

    // If we display all items without pager.
    if ($params['rows'] == 0) {
      $params['rows'] = 100000;
    }

    $this->query_params = $params;

    // Export parameters for preview.
    $view->build_info['query'] = var_export($params, TRUE);

  }

  /**
   * Executes the query and fills the associated view object with according
   * values.
   *
   * Values to set: $view->result, $view->total_rows, $view->execute_time,
   * $view->pager['current_page'].
   */
  public function execute(&$view) {
    try {
      $start = microtime(TRUE);

      // Execute the search.

      $data = new stdClass();

      $data = $this->fetch_results($this->options['solr_host'], $this->options['solr_port'], $this->options['solr_path'], $this->options['solr_timeout'], $this->options['solr_params'], $this->query_params);

      /* comment out unused params.
      // Add sorting.
      if (isset($this->params['sort'])) {
        $query->setAvailableSort($this->params['sort'][0], $this->params['sort'][1]);
        $query->setSolrsort($this->params['sort'][0], $this->params['sort'][1]);
      }

      $query->page = $this->pager->current_page;
       */

      // Store results.
      $view->result = $data->getDocuments();

      // Store the results.
      $this->pager->total_items = $view->total_rows = $data->getNumFound();
      $this->pager->update_page_info();

      // We shouldn't use $results['performance']['complete'] here, since
      // extracting the results probably takes considerable time as well.
      $view->execute_time = microtime(TRUE) - $start;
    }
    catch (Exception $e) {
      $this->errors[] = $e->getMessage();
    }

    if ($this->errors) {
      foreach ($this->errors as $msg) {
        drupal_set_message($msg, 'error');
      }
      $view->result = array();
      $view->total_rows = 0;
      $view->execute_time = 0;
      return;
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['solr_host'] = array('default' => '');
    $options['solr_port'] = array('default' => '');
    $options['solr_path'] = array('default' => '');
    $options['solr_timeout'] = array('default' => 5);
    $options['solr_params'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['solr_host'] = array(
      '#type' => 'textfield',
      '#title' => t('Solr host'),
      '#default_value' => $this->options['solr_host'],
      '#description' => t("The URL or IP to Solr host."),
      '#maxlength' => 1024,
    );
    $form['solr_port'] = array(
      '#type' => 'textfield',
      '#title' => t('Solr port'),
      '#default_value' => $this->options['solr_port'],
      '#description' => t("The port to the Solr host."),
      '#required' => TRUE,
    );
    $form['solr_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Solr path'),
      '#default_value' => $this->options['solr_path'],
      '#description' => t("The path to the Solr index."),
      '#required' => FALSE,
    );
    $form['solr_timeout'] = array(
      '#type' => 'textfield',
      '#title' => t('Solr timeout'),
      '#default_value' => $this->options['solr_timeout'],
      '#description' => t("The Solr connection timeout in seconds."),
      '#required' => FALSE,
    );
    $form['solr_params'] = array(
      '#type' => 'textfield',
      '#title' => t('Solr params'),
      '#default_value' => $this->options['solr_params'],
      '#description' => t("Additional raw query parameters to pass to Solr. (e.g. foo=bar&baz=boz)"),
      '#required' => FALSE,
    );
  }

  function add_field($table, $field, $alias = '', $params = array()) {
    $alias = $field;

    // Add field info array.
    if (empty($this->fields[$field])) {
      $this->fields[$field] = array(
      'field' => $field,
      'table' => $table,
      'alias' => $alias,
      ) + $params;
    }

    return $field;
  }

  function add_filter($filter) {
     $operator = $filter->operator;
     $solr_field = $filter->options['solr_field_filter'];

     // Account for solr query differences.
     switch ($operator) {
       case 'greaterthan':
         $value = '[' . $filter->value . ' TO *]';
         break;
       case 'lessthan':
         $value = '[* TO ' . $filter->value . ']';
         break;
       default:
         $value = $filter->value;
         break;
     }

     if ($filter->options['solr_field_urldecode']) {
       // Remove url-encoding characters which can conflict with solr syntax.
       $value = urldecode($value);
     }
     if (!($filter->options['solr_field_default'])) {
       $value = $solr_field . ':' . $value;
     }
     else {
       $this->params['default_field'] = $solr_field;
     }
     $this->params['filters'][] = $value;
  }

  function add_argument($argument) {
    $solr_field= $argument->options['solr_field'];
    $value = $argument->argument;
    $this->params['arguments'][] = $solr_field . ':' . $value;
  }

  function add_orderby($argument) {
    $solr_field = $argument->options['xpath_selector'];
    $this->params['sorts'][$solr_field] = strtolower($argument->options['order']);
  }

}
