<?php
/**
 * @file
 * Views hooks for views_solr_backend.
 */

/**
 * Implements hook_views_data().
 */
function views_solr_backend_views_data() {
  $data = array();
  $data['solr']['table']['group'] = t('Solr');

  $data['solr']['table']['base'] = array(
    'title' => t('Solr'),
    'help' => t('Queries an Solr index.'),
    'query class' => 'views_solr_backend',
  );

  $data['solr']['field'] = array(
    'title' => t('Solr field'),
    'help' => t('A field available in the schema.xml.'),
    'field' => array(
      'handler' => 'views_solr_backend_handler_field',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_solr_backend_handler_filter',
    ),
    'sort' => array(
      'handler' => 'views_solr_backend_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_solr_backend_handler_argument',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_plugins().
 */
function views_solr_backend_views_plugins() {
  return array(
    'query' => array(
      'views_solr_backend' => array(
        'title' => t('Solr'),
        'help' => t('Reads from an Solr index.'),
        'handler' => 'views_solr_backend_plugin_query',
      ),
    ),
  );
}
